package pl.sda;

import java.util.Scanner;

public class CricleMain {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj średnicę okręgu");
        float diam=sc.nextFloat();

        float result_first= (float) (diam*3.14);
        float result_math= (float)(diam*Math.PI);

        System.out.println("Obwód przy Pi=3,14 "+result_first);
        System.out.println("Obwód przy Pi Math "+result_math);

        System.out.println("Done!");

    }
}
